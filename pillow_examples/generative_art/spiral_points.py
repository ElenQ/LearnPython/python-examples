from PIL import Image, ImageDraw
import math as m

def sum_by_el( col1, col2 ):
    return tuple(map(lambda x, y: x + y, col1, col2))

def mult_val( col1, val ):
    return tuple(map(lambda x: x*val, col1))

def circle( center, radius ):
    plus  = mult_val((1,1), radius)
    minus = mult_val(plus, -1)
    return sum_by_el(center, minus) + sum_by_el(center, plus)


im = Image.new('RGBA', (1000,1000), color='black')
canvas = ImageDraw.Draw(im)

for i in range(1000):
    rads = i/2
    x = i * m.sin( rads ) + 500
    y = i * m.cos( rads ) + 500

    canvas.ellipse( circle((x,y), i/30), fill="white" )

del canvas
im.show()
