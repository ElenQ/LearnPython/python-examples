from PIL import Image, ImageDraw
import math as m


im = Image.new('RGBA', (1000,1000), color='black')
canvas = ImageDraw.Draw(im)

coords = [500,500]
for i in range(1000):
    rads = i/7
    x = i * m.sin( rads ) + 500
    y = i * m.cos( rads ) + 500

    coords += [x,y]

canvas.line(coords)
del canvas
im.show()
