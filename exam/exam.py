"""
# Evaluación práctica

1. Crea una Lista de los 100 primeros números naturales.

2. Crea una Lista de los 50 primeros números pares. Puedes usar la lista
   anterior como ayuda.

3. Crea una función que calcule la media de una lista de números y procesa
   las Listas del apartado 1 y 2 para demostrar que funciona.

4. Descarga el [dataset](http://www.football-data.co.uk/mmz4281/1718/SP1.csv) en
   formato CSV de `football-data.co.uk` y calcula las siguientes estadísticas:
   - Goles a favor del **Leganés**.
   - Cantidad de media de goles por partido. Puedes reutilizar la función del
     apartado 3.
"""


# Apartado 1
naturales = list(range(1, 101))

# Apartado 2
pares = list(range(2, 101, 2))

# Apartado 3
def avg( l ):
    return sum(l)/len(l)


# Apartado 4
import csv
with open("SP1.csv") as f:
    reader = csv.DictReader(f)
    data = tuple(reader)

    # Apartado 4.1
    legaHome = filter(lambda x: x['HomeTeam'].strip() == 'Leganes', data)
    legaAway = filter(lambda x: x['AwayTeam'].strip() == 'Leganes', data)
    goalsH   = map( lambda x: int(x['FTHG']), legaHome )
    goalsA   = map( lambda x: int(x['FTAG']), legaAway )

    goals = tuple(goalsH) + tuple(goalsA)

    leganes_favor = sum(goals) # Resultado 4.1

    # Apartado 4.2
    media_por_partido = avg( tuple(map(lambda x: int(x['FTHG'])+int(x['FTAG']), data)) )
