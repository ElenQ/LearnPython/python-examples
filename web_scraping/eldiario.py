import requests
from bs4 import BeautifulSoup

if __name__=="__main__":
    r = requests.get("https://www.eldiario.es/")
    if r.status_code != 200:
        print("Error en la consulta")
        exit()
    soup = BeautifulSoup(r.text, "html.parser")
    enlaces = soup.find(id="bd").find_all("a", href=True, class_="lnk")

    # Se queda con el primero, podría repetirse por todos con un bucle
    print(enlaces[0])
    el = enlaces[0]["href"]

    # Añade el dominio al inicio del link:
    el = "https://www.eldiario.es"+el
    print(el)

    # Accede al contenido del enlace
    r = requests.get(el)
    print(r.status_code) # Tiene que dar un OK

    # Procesa el contenido HTML
    s = BeautifulSoup(r.text, "html.parser")

    # Encuentra y visualiza el titular
    print(s.find("h1", class_="pg-headline").text)
