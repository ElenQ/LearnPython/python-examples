# -*- coding: utf-8 -*-
import scrapy


class LoteriasSpider(scrapy.Spider):
    name = 'loterias'
    allowed_domains = ['loteriasyapuestas.es']
    start_urls = ['https://www.loteriasyapuestas.es/es/la%2Dquiniela/sorteos/2018/1027006036']

    def parse(self, response):
        home =   [ x.strip() for x in response.xpath('//div[@class="cuerpoRegionLeft"]/ul[2]/li/text()').extract() ]
        away =   [ x.strip() for x in response.xpath('//div[@class="cuerpoRegionLeft"]/ul[3]/li/text()').extract() ]
        result = [ x.strip() for x in response.xpath('//div[@class="cuerpoRegionRight"]/ul[2]/li/text()').extract() ]
        jornada = response.xpath('//div[@class="tituloRegion"]/h3/text()').re(r'.*?(\d+)')[0]
        year = response.xpath('//div[@class="cabeceraRegion"]/div[@class="tituloRegion"]/h2/text()').re(r'.*?(\d+)$')[0]
        #yield {"jornada": jornada, "home": home, "away": away, "result": result, "year": year}

        for (row, d) in enumerate( zip(home, away, result) ):
            h, a, r = d
            yield { "row": row + 1,
                    "home": h,
                    "away": a,
                    "result": r,
                    "jornada": jornada,
                    "year": year }

        nextpage = response.xpath('//div[@class="resultadoAnterior"]/a/@href').extract_first()
        if nextpage is not None:
            yield response.follow(nextpage)
