"""
4. Descarga el [dataset](http://www.football-data.co.uk/mmz4281/1718/SP1.csv) en
   formato CSV de `football-data.co.uk` y calcula las siguientes estadísticas:
   - Goles a favor del **Leganés**.
   - Cantidad de media de goles por partido. Puedes reutilizar la función del
     apartado 4.
"""

import pandas as pd

data = pd.read_csv("SP1.csv")

home_lega = data["HomeTeam"].str.strip() == "Leganes"
away_lega = data["AwayTeam"].str.strip() == "Leganes"

hl = data[home_lega][["HomeTeam","FTHG"]].rename(columns={"HomeTeam": "Team", "FTHG":"Goals"})
al = data[away_lega][["AwayTeam","FTAG"]].rename(columns={"AwayTeam": "Team", "FTAG":"Goals"})

print( al.append(hl, ignore_index=True).groupby("Team").sum() )



goals = data["FTHG"] + data["FTAG"]
print( goals.mean() )
