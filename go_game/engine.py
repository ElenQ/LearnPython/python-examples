def are_neighbors( a, b ):
    """ Comprueba si dos piedras son vecinas.
    Devuelve True si lo son, False si no lo son
    """
    if a["x"] == b["x"]:
        return abs(a["y"]-b["y"]) == 1 # Ojo con este truco ;)
    if a["y"] == b["y"]:
        return abs(a["x"]-b["x"]) == 1
    return False

def get_neighbors( board, stone ):
    """ Filtra el trablero y obtiene las vecinas de la piedra que se le diga.
    Devuelve una lista de piedras.
    """
    return list( filter(lambda x: are_neighbors(stone, x), board) )

def get_group( board, stone ):
    """ Obtiene el grupo al que pertenece la piedra.
    Devuelve una lista de piedras.
    """
    group = []                    # Lista de piedras en el grupo
    unprocessed   = [ stone ]     # Lista de ayuda, guarda las no-procesadas
    while len(unprocessed) > 0:   # Mientras que queden piedras sin procesar

        # Se coge una piedra y se pasa a la lista de piedras procesadas
        current_stone = unprocessed.pop()
        group.append( current_stone )

        # Se obtienen las vecinas del mismo color
        neighbors  =  get_neighbors( board, current_stone )
        same_color = filter(lambda x: x["color"]==current_stone["color"], neighbors)
        for i in same_color:
            if i not in group:
                # Si la vecina del mismo color no está procesada ya, se añade a
                # la lista de piedras sin procesar.
                unprocessed.append( i )
    return group  # Cuando se han procesado todas se devuelve el grupo entero

def count_liberties( board, group, size ):
    """ Cuenta las libertades de un grupo para un tamaño de tablero.
    Devuelve el número de libertades (integer).
    """
    # Inicializa las libertades totales
    liberties = 0

    # Cuenta las libertades de cada piedra del grupo
    for stone in group:
        # Para empezar son 4 menos el número de vecinas
        liberties += 4 - len( get_neighbors( board, stone ) )

        # Menos 1 por cada borde del tablero que toque
        if stone["x"] == 0 or stone["x"] == size-1:
            liberties -= 1
        if stone["y"] == 0 or stone["y"] == size-1:
            liberties -= 1

    # Se acumulan en el total y se devuelve el total
    return liberties

def without_group( board, group ):
    """ Crea un nuevo tablero sin el grupo que se le diga.
    Devuelve un tablero nuevo (una lista de piedras).
    """

    # Simplemente filtra las que no pertenezcan al grupo
    return list( filter( lambda x: x not in group, board) )

def put_stone( board, stone, size ):
    # Copia el tablero y le añade la nueva piedra
    processed_board = [ dict(i) for i in board ] + [stone]

    # Captura las piedras de alrededor
    neighbors = get_neighbors( processed_board, stone )

    other_color = list( filter(lambda x: x["color"]!=stone["color"], neighbors) )

    # Procesa las conquistas
    for n in other_color:
        group = get_group( processed_board, n )
        if count_liberties( processed_board, group, size ) == 0:
            processed_board = without_group( processed_board, group )

    # Procesa el suicidio
    # NOTE: este bloque junto con el anterior se pueden mover a
    # una función
    same_group = get_group( processed_board, stone )
    if count_liberties( processed_board, same_group, size ) == 0:
        processed_board = without_group( processed_board, same_group )

    return processed_board


if __name__ == "__main__":
    # TODO Cambiar esto y hacer pruebas ejercicio para casa.
    pass
