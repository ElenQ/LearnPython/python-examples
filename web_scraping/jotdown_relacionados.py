import requests
from bs4 import BeautifulSoup

# Tree processing library, very simple
from anytree import Node, RenderTree
from anytree.search import find

def get_related(url):
    r = requests.get(url)
    soup = BeautifulSoup(r.text, "html.parser")
    return [ (rel["href"], rel.string) for rel in soup.select(".related_post a.wp_rp_title") ]

def follow_tree(root, branch, url):
    if branch.depth >2:
        return

    for link, title in get_related(url):
        search = find(root, lambda node: node.name == link)
        if search:
            continue
        new_branch = Node(title, parent=branch, link=link)
        follow_tree(root, new_branch, link)

if __name__ == "__main__":
    url = "https://www.jotdown.es/2019/05/usar-pollo-de-goma-con-polea-en-medio-contra-cabra-infame/"
    title = BeautifulSoup(requests.get(url).text, "html.parser").select("h1.title a")[0].string
    root = Node(title, link=url)

    follow_tree(root, root, url)

    for pre, fill, node in RenderTree(root):
        print("%s%s" % (pre, node.name))

# RESULT OF THE SCRIPT:
#
# Usar pollo de goma con polea en medio contra cabra infame
# ├── Jugar a la risa
# │   ├── Usar pollo de goma con polea en medio contra cabra infame
# │   │   ├── Jugar a la risa
# │   │   ├── No queremos que copies esto
# │   │   ├── Vida de Lucasfilm Games, muerte de LucasArts (y II)
# │   │   ├── Vida de Lucasfilm Games, muerte de LucasArts (I)
# │   │   └── El cerebro de la bestia: el nacimiento de Super Nintendo (I)
# │   ├── No queremos que copies esto
# │   │   ├── Vida de Lucasfilm Games, muerte de LucasArts (I)
# │   │   ├── Vida de Lucasfilm Games, muerte de LucasArts (y II)
# │   │   ├── Los mejores juegos que nunca han existido
# │   │   ├── Debería estar en un museo: portadas de videojuegos
# │   │   └── Jugar a la risa
# │   ├── Vida de Lucasfilm Games, muerte de LucasArts (I)
# │   │   ├── Vida de Lucasfilm Games, muerte de LucasArts (y II)
# │   │   ├── En medio del Caribe… monos con tres cabezas
# │   │   ├── No queremos que copies esto
# │   │   ├── Los mejores juegos que nunca han existido
# │   │   └── A la caza del huevo de Pascua II: Videogame Edition
# │   ├── Vida de Lucasfilm Games, muerte de LucasArts (y II)
# │   │   ├── Vida de Lucasfilm Games, muerte de LucasArts (I)
# │   │   ├── Los mejores juegos que nunca han existido
# │   │   ├── No queremos que copies esto
# │   │   ├── En medio del Caribe… monos con tres cabezas
# │   │   └── Usar pollo de goma con polea en medio contra cabra infame
# │   └── Retorcidos villanos: los enemigos más extraños del videojuego
# │       ├── El cerebro de la bestia (y II): hemos venido a jugar
# │       ├── No queremos que copies esto
# │       ├── Los mejores juegos que nunca han existido
# │       ├── Solo se muere tres veces
# │       └── Nintendo Classic Mini y la máquina del tiempo
# ├── No queremos que copies esto
# │   ├── Vida de Lucasfilm Games, muerte de LucasArts (I)
# │   │   ├── Vida de Lucasfilm Games, muerte de LucasArts (y II)
# │   │   ├── En medio del Caribe… monos con tres cabezas
# ...
