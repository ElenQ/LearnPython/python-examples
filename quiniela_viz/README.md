# Quiniela Viz

This example takes the output code from the `scrapy_quiniela` example and it
visualize the result in a Quiniela paper.

It requires `pandas` and `pillow`.
