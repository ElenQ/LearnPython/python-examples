import pandas as pd
import numpy  as np
import matplotlib.pyplot as plt

"""
This program uses the dataset that can be found here:

    https://www.kaggle.com/gremlin97/predicting-melbourne-housing-prices/data

"""

DATA = pd.read_csv("melb_data.csv", parse_dates=['Date'])

def price_vs_pos(data):
    price_vs_loc = data[["Lattitude", "Longtitude", "Price"]]
    price_vs_loc.plot.scatter(x="Longtitude", y="Lattitude", c="Price")
    plt.show()
    return price_vs_loc

def price_vs_pos_hex(data):
    price_vs_loc = data[["Lattitude", "Longtitude", "Price"]]
    price_vs_loc.plot.hexbin(x="Longtitude", y="Lattitude", C="Price", gridsize=50)
    plt.show()
    return price_vs_loc

def price_per_room_vs_suburb(data):
    data["PerRoom"] = data["Price"] / data["Rooms"].astype(float)
    values = data[["PerRoom", "Price", "Suburb"]].groupby("Suburb").PerRoom.agg([np.mean, np.std]).sort_values(by="mean")
    values.plot()
    plt.show()
    return values

def price_evolution_per_suburb(data):
    # TODO
    p_d = data[["Price", "Date", "Suburb"]]
    v = p_d.groupby( [data["Date"].dt.year, data["Date"].dt.month, "Suburb"] ).Price.agg([np.mean])
    return v.loc[v.index.get_level_values('Suburb') == "Surrey Hills"]

price_vs_pos(DATA)
price_vs_pos_hex(DATA)
price_per_room_vs_suburb(DATA)
# print( price_evolution_per_suburb(DATA) )

