from matplotlib import pyplot as plt
import pandas as pd

"""
This program makes use of this dataset:
http://www.bilbao.eus/opendata/es/catalogo/dato-habitantes-distrito-barrio-edad-2018

Can be downloaded directly from this link:
www.bilbao.eus/bilbaoopendata/demografia/numero_habitantes_distrito_barrio_edad_2018.csv
"""

# Leemos el CSV, poned bien la ruta, fijáos en los separadores y en el
# encoding!
# El separador limpia los espacios
df = pd.read_csv("numero_habitantes_distrito_barrio_edad_2018.csv",sep=" *; *", encoding='latin-1', engine='python')

# Quitamos la última fila, porque aparecen los totales
# https://chrisalbon.com/python/data_wrangling/pandas_dropping_column_and_rows/
df = df[0:-1]

# Quitamos columnas que no nos interesan
# Pueden quitarse con:
# del df["COLUMNA"]
#
# Ojo al axis también:
# https://stackoverflow.com/questions/22149584/what-does-axis-in-pandas-mean#22149930
df = df.drop(["COD. BARRIO", "COD. DISTR.", "COD. BARRIO", "FEC_OFI_AYTO"], axis="columns")


def numberify(name):
    """
    Esta funcion procesa los títulos de las columnas y los convierte a integer
    retirándoles la parte en la que dice "AÑO" o "AÑOS"
    """
    if "AÑO" in name:
        return int( name.split(" ")[0] )
    else:
        return name

district_total = df.groupby(["DISTRITO", "SEXO"]).sum()          # Recordar que agg(np.sum) es equivalente, pero esto es más rápido
district_total = district_total.rename(columns=numberify)        # Cambiamos los nombres a número de años
transposed = district_total.loc[("ABANDO", slice(None))].T[0:-2] # Transpone y elimina el total y el Mayores de 110.

# Unas visualizaciones
transposed.plot()
transposed.plot(kind="bar")

plt.show()      # Visualiza los plots que han quedado encolados en las llamadas a plot
