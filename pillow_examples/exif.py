import PIL.Image
import PIL.ExifTags
import sys

if __name__== "__main__":
    for i in sys.argv[1:]:
        img = PIL.Image.open(i)
        try:
            exif = {
                PIL.ExifTags.TAGS[k]: v
                for k, v in img._getexif().items()
                if k in PIL.ExifTags.TAGS
            }
        except:
            print(i, "No exif data")
            continue


        try:
            print(i, exif["DateTimeOriginal"])
        except:
            print(i, "No date included")

