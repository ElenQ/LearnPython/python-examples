# Quiniela crawler

This project crawls Loterías y Apuestas del Estado website and crawls all the
quinielas since the end of the 2017-2018 season.

Run it like:

``` bash
cd quiniela_crawler
scrapy crawl loterias -o data.csv       # Will export data in CSV file
```

Stop the scraper anytime you like with `Ctrl+c`, it will never end and parse
until it doesn't find anything else.

The result can be exported in many different formats. Change the format with
the extension of the output file. `.json` for JSON, `.jl` for JSON lines,
`.csv` for CSV files...
