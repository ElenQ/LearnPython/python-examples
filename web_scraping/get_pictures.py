import requests
from bs4 import BeautifulSoup

# This simple script gets all the pictures of the link and downloads them to
# the local drive.
# It's not intended to be a great solution, just to see some recipes to use on
# web scrapping with Requests and BeautifulSoup4

def filename(l):
    """
    Example:
    - INPUT: https://dominio.com/image.jpg?size=large
    - OUTPUT: image.jpg
    """
    return l.split("/")[-1].split("?")[0]

if __name__ == "__main__":

    # Make the HTTP GET request
    r = requests.get("https://www.nytimes.com/2019/04/09/world/middleeast/israel-election-results.html")

    # Create the HTML Parser
    soup = BeautifulSoup(r.text, "html.parser")

    # Get the `src` attribute of all images:
    # It's a list comprehension, see:
    #   https://docs.python.org/2/tutorial/datastructures.html#list-comprehensions
    links = [ im["src"] for im in soup.find("img") ]

    # Iterate in all the links and download the images
    for l in links:

        # Request each link
        r = requests.get(l)

        # Open each file and store it "wb" means Write Binary
        with open( filename(r.url), "wb") as f:
            f.write(r.content)
