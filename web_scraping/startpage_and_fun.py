import requests
from bs4 import BeautifulSoup
import re
from multiprocessing.dummy import Pool as ThreadPool

def search_page(query, page=0):
    post_data = {
            "language": "english",
            "query": (query + " magnet torrent descargar"),
            "page": page
            }

    headers_data = {
            "User-Agent": "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:67.0) Gecko/20100101 Firefox/67.0"
            }

    r = requests.post("https://www.startpage.com/do/search",
            data=post_data,
            headers=headers_data)

    # https://2.python-requests.org/en/master/user/quickstart/#response-status-codes
    r.raise_for_status()

    soup = BeautifulSoup(r.text, "html.parser")

    results_processed = []
    for result in soup.find_all(class_="w-gl__result"):
        results_processed.append({ "link": result.a["href"],
                                   "title": result.a.string })
    return results_processed


def search_magnet_link(url):
    print("Checking:", url)
    try:
        r = requests.get(url, timeout=2)
    except:
        return []

    if not r.headers["Content-Type"].startswith("text/html"):
        return []

    soup = BeautifulSoup(r.text, "html.parser")
    magnet_links = soup.find_all( href = re.compile("magnet\:.*") )
    return [magnet_link["href"] for magnet_link in magnet_links]


# These two calls act the same way, they return a structure with this shape:
#
# [ ( ["found_magnet1", "found_magnet2"...] , {"link": ..., "title": ...} ),
#   ( [...] , {...}),
#   ...
# ]
def call_sequentially(search_results):
    results = []
    for i in search_results:
        results.append( (search_magnet_link( i["link"] ), i) )
    return results

def call_multithread(search_results):
    pool = ThreadPool(10)
    results = pool.map( lambda x: (search_magnet_link(x["link"]), x), search_results )
    pool.close()
    pool.join()
    return results


if __name__ == '__main__':

    query  = "Juego de tronos"
    seen   = 0
    found_links = []
    lastpage = 1

    search_results = search_page(query, lastpage)
    while True:
        seen += len(search_results)


        # Choose sequential or multithread method to see the difference
        #call_function = call_sequentially
        call_function = call_multithread

        for i in call_function(search_results):
            if not i[0]:
                continue
            found_links += i[0]
            print("Found links at:", i[1]["link"])
            for j in i[0]:
                print("\t" + j)

        # Found something?
        if len(found_links) != 0:
            break
        else:
            print("[INFO]: Not links found in first %d links, checking next page" % seen)
        lastpage = lastpage+1
        search_results = search_page(query, page=lastpage)
