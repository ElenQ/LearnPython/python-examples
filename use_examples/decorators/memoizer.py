import pickle
from collections import OrderedDict

"""
This is the worst memoization decorator in the world, but works for any
function.
Consider it an exercise to learn about decorators.
"""

class Memoize:
    def __init__(self, size):
        self.size = size
        self.cache = OrderedDict()

    def __call__(self, fn):
        def _ (*args, **kwargs):
            print(self.cache)
            input_args = pickle.dumps((args, kwargs))
            if input_args in self.cache:
                print("Hit!")
                return self.cache[input_args]
            else:
                output = fn(*args, **kwargs)

                if len(self.cache) >= self.size:
                    self.cache.popitem(last=False)
                self.cache[input_args] = output
                return output
        return _



@Memoize(2)
def test(a, b):
    return a + b

@Memoize(10)
def test2(a, b):
    return a * b


if __name__ == "__main__":
    print(test(1,2))
    print(test(1,2))
    print(test(4,2))
    print(test(3,2))
    print(test(6,2))
    print(test(1,4))
    print(test(1,5))
    print(test(2,2))
    print(test(4,3))
    print(test(3,3))
