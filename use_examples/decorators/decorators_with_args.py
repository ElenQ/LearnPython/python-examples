def deco_creator (*args, **kwargs):
    print("Args on creation: ")
    print(">> ", args)
    print(">> ", kwargs)

    def decorator(fn):
        print("Decorating")
        def new_func(*args, **kwargs):
            print("Called decoration")
            return fn(*args, **kwargs)
        return new_func
    return decorator

@deco_creator("arg1", "arg2", "...", opts="thing")
def function_to_decorate( a, b, c):
    print("Called function with: ", a, b, c)

function_to_decorate(1,2,3)

# Explicación:
# @deco_creator(...)
# def function
#
# equivale a:
# function = deco_creator(...)(function)
#            ^^^^^^^^^^^^^^^^^ Devuelve una función
#                             ^^^^^^^^^^ llamada a la función devuelta

######################################################################
print("\n")

class DecoCreator:
    "Puede hacerse en clase o en función"
    def __init__(self, *args, **kwargs):
        print("Args on creation: ")
        print(">> ", args)
        print(">> ", kwargs)

    def __call__(self, fn):
        print("Decorating")
        def new_func(*args, **kwargs):
            print("Called decoration")
            return fn(*args, **kwargs)
        return new_func

@DecoCreator("arg1", "arg2", "...", opts="thing")
def function_to_decorate( a, b, c):
    print("Called function with: ", a, b, c)

function_to_decorate(1,2,3)

# Explicación:
# @DecoCreator(...)
# def function
#
# equivale a:
# function = DecoCreator(...)(function)
#            ^^^^^^^^^^^ Nombre de clase
#            ^^^^^^^^^^^^^^^^ Instanciación de objeto: llamada a __init__
#                            ^^^^^^^^^^ llamada a la instancia: método __call__

# MEMOIZADME CON UN DECORATOR USANDO ARGUMENTOS:
# - Tamaño de cache seleccionable
#
