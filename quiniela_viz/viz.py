import pandas as pd
from PIL import Image, ImageDraw

# This global data is taken from the picture by hand
Q = {
        "left": 375,
        "top": 183,

        "height": 212-183,
        "width": 396-376,

        "padding-bottom": 10.1,
        "padding-right": 404-395+0.5,

        "filename": "papelote_blur_rot.jpg",
    }
df = pd.read_csv("data.csv")

# Discard quinielas with missing data
df = df.groupby("jornada").filter(lambda x: x["result"].count() == 15)

# Discard `pleno al quince`
df = df[df["row"]!= 15]

# Create the canvas
im = Image.open(Q["filename"])
draw = ImageDraw.Draw(im)

bypos = df.groupby("row")
for quinielaPos, group in bypos:
    total = group["result"].count()
    res = group.groupby("result").count()["row"]
    x0 = {
            "1": Q["left"],
            "X": Q["left"] + Q["width"] + Q["padding-right"],
            "2": Q["left"] + 2*Q["width"] + 2*Q["padding-right"] }
    x1 = { x: x0[x] + Q["width"] for x in x0 }

    y1_all = Q["top"] + quinielaPos * Q["height"] + (quinielaPos - 1) * Q["padding-bottom"]
    y1 = {
            "1": y1_all,
            "X": y1_all,
            "2": y1_all }
    y0 = { y: y1[y] - float(res[y])/total * Q["height"] for y in y1 }

    print(x0,x1,y0,y1)
    for r in ["1", "X", "2"]:
        rect = [x0[r], y0[r], x1[r], y1[r]]
        draw.rectangle(rect, fill="#6AB0DE", outline="black")
del draw
im.save("quiniela_bars.jpg")
