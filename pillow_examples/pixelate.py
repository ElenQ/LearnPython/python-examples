from PIL import Image, ImageDraw
from math import floor, ceil

"""
This module pixelates an image.
First the image is cropped to fit a square grid. Then, each square is replaced
with the average color of the square.

NOTES:

    The result is just shown but not stored in disk.

    There's no input handling, everything is hardcoded in the main. Change the
    parameters there or figure out how to use input arguments.
"""


def round_down(numb):
    return int(floor(numb))

def round_up(numb):
    return int(ceil(numb))

def crop_to_multiple( image, size ):
    image_size = image.size

    # Magic trick for the case the remainder's half is not integer.
    # Figure out by yourselves how it works.
    rest = list(map( lambda s: float(s % size)/2,  image_size ))
    rlow = list(map( round_up,   rest ))
    rupp = list(map( round_down, rest ))

    box = (rlow[0], rlow[1], image_size[0]-rupp[0], image_size[1]-rupp[1])
    return image.crop(box)

def get_medium_color( image ):
    band_avg = []
    for band in range(3):
        data = tuple(image.getdata( band ))
        avg  = round_down(sum(data)/len(data))
        band_avg.append( avg )

    return tuple(band_avg)

if __name__ == "__main__":

    # Hardcoded values, better load them from input arguments.
    # Read about arguments. `sys.argv` is a list that stores them.
    # There are useful modules to parse them: getopt, argparse and more.
    STEP_SIZE = 60
    MAIN_IMAGE = Image.open("prueba.jpg")
    MAIN_IMAGE.show()

    MAIN_IMAGE = crop_to_multiple(MAIN_IMAGE, STEP_SIZE)
    MAIN_IMAGE.show()
    sizeX, sizeY = MAIN_IMAGE.size
    canvas = ImageDraw.Draw(MAIN_IMAGE)

    for y in range(0, sizeY, STEP_SIZE):
        for x in range(0, sizeX, STEP_SIZE):
            box = ( x, y, x+STEP_SIZE, y+STEP_SIZE )
            piece =  MAIN_IMAGE.crop( box )
            color = get_medium_color( piece )

            canvas.rectangle( box, fill=color )

    del canvas # learn about del operator
    MAIN_IMAGE.show()
    # Doesn't save the image in disk
